package com.roshgolla.learning.scala.cats.HybridParIO

import cats.effect.IO
import com.typesafe.scalalogging.LazyLogging

class DataStore extends LazyLogging {
  def generate[T](x: T, format: String, delay: Int): IO[T] = IO {
    logger.info(s"generating a $format with delay $delay")
    Thread.sleep(delay)
    logger.info(s"generated $format")
    x
  }
}

object DataStore extends LazyLogging {
  def apply(): DataStore = {
    logger.info(s"creating instance")
    new DataStore()
  }
}
