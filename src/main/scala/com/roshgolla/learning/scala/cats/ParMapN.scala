package com.roshgolla.learning.scala.cats

import cats.data._
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging

object ParMapN extends IOApp with LazyLogging {

  override def run(args: List[String]): IO[ExitCode] =  for {
    _ <- IO(logger.info(s"starting"))
    _ <- joiner()
  } yield ExitCode.Success

  case class Input(x: Int, y: Int, delay: Int)

  private def joiner(): IO[Unit] = {
    NonEmptyList.of(
      Input(1, 1, 1000),
      Input(2, 2, 5000),
      Input(3, 3, 500),
    ).parTraverse { input =>
      IO.shift *> sum(input)
    }.map { f =>
      logger.info(s"finished all: $f")
    }
  }

  private def sum(input: Input): IO[Int] = IO {
    logger.info(s"calculating sum for input $input")
    Thread.sleep(input.delay)
    val result = input.x + input.y

    logger.info(s"input = $input, result = $result")
    result
  }
}
