package com.roshgolla.learning.scala.cats.HybridParF

import cats.Applicative
import cats.effect.Concurrent
import cats.implicits._
import com.typesafe.scalalogging.LazyLogging

class Program[F[_] : Concurrent : DataStore]() extends LazyLogging {

  private val dataStore = DataStore[F]
  private val concurrent = Concurrent[F]

  def run(): F[Unit] = {
    val intGenerator = dataStore.generate(5, "int", 1500)
    val stringGenerator = dataStore.generate("Five", "string", 500)

    for {
      intFiber <- concurrent.start(intGenerator)
      stringFiber <- concurrent.start(stringGenerator)

      int <- intFiber.join
      string <- stringFiber.join

      result <- Applicative[F].pure(List(int, string))
    } yield logger.info(s"result: $result")
  }
}
