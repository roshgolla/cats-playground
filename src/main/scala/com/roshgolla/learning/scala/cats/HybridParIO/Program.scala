package com.roshgolla.learning.scala.cats.HybridParIO

import cats.effect.{ContextShift, IO}
import com.typesafe.scalalogging.LazyLogging

class Program(dataStore: DataStore)(implicit cs: ContextShift[IO]) extends LazyLogging {
  def run(): IO[Unit] = {
    val intGenerator: IO[Int] = dataStore.generate(5, "int", 1500)

    val stringGenerator: IO[String] = dataStore.generate("Five", "string", 500)

    for {
      intFiber <- intGenerator.start
      stringFiber <- stringGenerator.start
      int <- intFiber.join
      string <- stringFiber.join
      result <- IO.pure(List(int, string))
    } yield logger.info(s"result = $result")
  }
}

object Program extends LazyLogging {
  def apply(dataStore: DataStore)(implicit cs: ContextShift[IO]): Program = {
    logger.info(s"creating instance")
    new Program(dataStore)
  }
}
