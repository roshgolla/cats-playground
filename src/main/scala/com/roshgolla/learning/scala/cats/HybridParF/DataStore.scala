package com.roshgolla.learning.scala.cats.HybridParF

import cats.effect.Sync
import com.typesafe.scalalogging.LazyLogging

trait DataStore[F[_]] {
  def generate[T](x: T, format: String, delay: Int): F[T]
}

object DataStore extends LazyLogging {
  def apply[F[_]]()(implicit dataStore: DataStore[F]): DataStore[F] = {
    logger.info(s"creating instance")
    dataStore
  }
}

class DataStoreInMemory[F[_] : Sync] extends DataStore[F] with LazyLogging {
  def generate[T](x: T, format: String, delay: Int): F[T] = Sync[F].delay {
    logger.info(s"generating a $format with delay $delay")
    Thread.sleep(delay)
    logger.info(s"generated $format")
    x
  }
}
