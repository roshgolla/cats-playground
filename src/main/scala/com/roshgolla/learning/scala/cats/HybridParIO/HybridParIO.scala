package com.roshgolla.learning.scala.cats.HybridParIO

import cats.effect.{ExitCode, IO, IOApp}
import com.typesafe.scalalogging.LazyLogging

/**
 * Run two IO operations of different return types in two different threads.
 * When both are finished use results of both and return combined results
 */

object HybridParIO extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    Program(DataStore())
      .run() *> IO.pure(ExitCode.Success)
  }
}
