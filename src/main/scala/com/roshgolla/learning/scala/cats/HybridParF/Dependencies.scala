package com.roshgolla.learning.scala.cats.HybridParF

import cats.effect.IO

object Dependencies {
  implicit lazy val dataStoreInMemory: DataStoreInMemory[IO] = new DataStoreInMemory[IO]
  implicit lazy val dataStore: DataStore[IO] = DataStore()
}
