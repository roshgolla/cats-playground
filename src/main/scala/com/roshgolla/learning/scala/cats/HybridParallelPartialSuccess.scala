package com.roshgolla.learning.scala.cats

import cats.effect.{ExitCode, IO, IOApp}
import com.typesafe.scalalogging.LazyLogging

/**
 * Run two IO operations of different return types in two different threads.
 * One will succeed and one will fail.
 * When one fails cancel other one and return error
 */

object HybridParallelPartialSuccess extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    main() *> IO.pure(ExitCode.Error)
  }

  private def main(): IO[Unit] = {
    val ioOne: IO[Int] = IO {
      logger.info(s"ioOne start but it should fail eventually")
      Thread.sleep(500)
    } *> IO.raiseError(new RuntimeException)

    val ioTwo: IO[String] = IO {
      logger.info(s"generating string")
      Thread.sleep(2000)
      logger.info(s"generated string")
      "Five"
    }

    for {
      intFiber <- ioOne.start
      stringFiber <- ioTwo.start
      _ <- ioOne.handleErrorWith(cause =>
        stringFiber.cancel *> IO.raiseError(new RuntimeException("cancelled ioTwo as ioOne failed", cause))
      )
      int <- intFiber.join
      string <- stringFiber.join
      result <- ioCombine(int, string)
    } yield logger.info(s"result = $result")
  }

  private def ioCombine(x: Int, y: String): IO[List[Any]] = IO.pure(List(x, y))
}
