package com.roshgolla.learning.scala.cats.HybridParF

import cats.effect.{ExitCode, IO, IOApp}
import com.roshgolla.learning.scala.cats.HybridParF.Dependencies._
import com.typesafe.scalalogging.LazyLogging

/**
 * Using Higher Kinded Type
 * re-implement `HybridParallel` program
 */

object HybridParF extends IOApp with LazyLogging {
  override def run(args: List[String]): IO[ExitCode] = {
    new Program[IO]()
      .run() *> IO.pure(ExitCode.Success)
  }
}
