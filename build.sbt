name := "cats-playground"

version := "0.1"

scalaVersion := "2.12.11"

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "org.typelevel" %% "cats-effect" % "2.1.3",
  "org.typelevel" %% "cats-core" % "1.0.0",
  "org.typelevel" %% "simulacrum" % "1.0.0"
)

scalacOptions ++= Seq(
  "-Ypartial-unification",
  "-language:higherKinds",
)

Compile / console / scalacOptions --= Seq(
  "-deprecation",
  "-Xfatal-warnings",
  "-Xlint"
)
